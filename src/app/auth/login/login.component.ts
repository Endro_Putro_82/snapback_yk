import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
user:any={};
  constructor(
    public api:ApiService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  hide: boolean=true;

  loading: boolean | undefined;
  login()
  {
    this.loading=true;
    this.api.login(this.user.email, this.user.password).subscribe(res=>{
      localStorage.setItem('appToken',JSON.stringify(res));
      this.loading=false;
      this.router.navigate(['admin/dashboard']);
    },error=>{
      this.loading=false;
      alert('Tidak dapat login');
    })
  }
}
